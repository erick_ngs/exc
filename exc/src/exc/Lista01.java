package exc;

import java.util.Scanner;

//EXC 01
public class Lista01 {

	static Scanner tecla = new Scanner(System.in);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub

		//Vari�veis locais


		double raio, area=0;
		raio=0;

		//Entrada de dados

		System.out.println("Informe o raio do c�rculo: ");
		raio = tecla.nextDouble();

		//Processamento de dados

		area = calcularArea(raio);


		//Sa�da

		System.out.println("Valor da �rea: " + area);

		}

		//Toda fun��o deve ser "static"
		//Para ser usada em outra classe, deve ser declarada como "public"

		/**
		* {@value M�todo para calculo de raio.}
		* @param raio
		* @return
		* Coment�rios para essa classe, que aparecer� toda vez que for declarada.
		*/

		private static double calcularArea(double raio) {
		// TODO Auto-generated method stub
		return Math.PI * Math.sqrt(raio);


	}

}
