package exc;

import java.util.Scanner;

public class Lista04 {

	static Scanner av1 = new Scanner(System.in);
	static Scanner av2 = new Scanner(System.in);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		double nota1=0, nota2=0, media=0;
		
		System.out.println("Nota da AV1: ");
		nota1 = av1.nextDouble();
		
		System.out.println("Nota da AV2: ");
		nota2 = av2.nextDouble();
		
		media = (nota1 + nota2) / 2;
		
		if (media >= 6) {
			System.out.println("APROVADO");
			System.out.println("MEDIA: " + media);
		} else {
			System.out.println("REPROVADO");
			System.out.println("MEDIA: " + media);
		}
	}

}
