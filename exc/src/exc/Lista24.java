package exc;

import java.util.Scanner;

public class Lista24 {

	static Scanner ml = new Scanner(System.in);
	static Scanner nl = new Scanner(System.in);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int numL=0;
		double mediaL=0;
		
		System.out.println("QTD DE LADOS DO POILIGONO: ");
		numL = nl.nextInt();
		
		System.out.println("MEDIDA DO LADO(EM CM): ");
		mediaL = ml.nextDouble();
		
		if (numL == 3) {
			System.out.println("TRIANGULO");
			System.out.println("PERIMETRO: " + (mediaL * 3));
		} else if (numL == 4) {
			System.out.println("QUADRADO");
			System.out.println("AREA: " + (mediaL * mediaL));
		} else{
			System.out.println("PENTAGONO");
		}
		
	}

}
